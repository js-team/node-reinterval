var reInterval = require('reinterval');

var inter = reInterval(function () {
    console.log('this should be called after 13s');
    process.exit(0);
}, 10 * 1000);
 
// This will reset/reschedule the interval after 3 seconds, therefore
// the interval callback won't be called for at least 13 seconds.
setTimeout(function () {
    inter.reschedule(10 * 1000);
}, 3 * 1000);
